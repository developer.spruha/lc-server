class GameTable {
    constructor(tableId = '', players = [], deck = [], limit = 2) {
        this.tableId = tableId
        this.players = players
        this.droppedCards = []
        this.history = []
        this.joker = []
        this.deck = deck
        this.turn = null
        this.limit = limit
    }

    addPlayerToTable (player) {
        this.players.push(player.socket)
        return players
    }

    selectJoker () {
        this.joker = this.deck.drawCard()
        // console.log('Joker for the table is: ', this.joker)
        return this.joker
    }

    dealCards () {
        for (let i = 0; i < 7; i++) {
            this.players.forEach((player) => {
                player.dealCard(this.deck.drawCard())
            })
        }
        // console.log(`${this.players[0].username} has a hand of: `, this.players[0].hand)
        // console.log(`${this.players[1].username} has a hand of: `, this.players[1].hand)
        return
    }

    dropCard (card) {
        this.droppedCards.push(card)
        return
    }

    toggleTurn () {
        let tableOrder = this.sendTableOrder(this.turn.socket)
        for (let i = 0; i < tableOrder.length; i++){
            if (tableOrder[i].tableStatus){
                this.turn = tableOrder[i]
                break
            }
        }
        return this.turn
    }

    sendTableOrder (socketID) {
        let playerSeat = this.players.filter((player) => player.socket === socketID)[0].seat
        let firstHalf = this.players.filter((otherPlayer) => otherPlayer.seat < playerSeat)
        let secondHalf = this.players.filter((otherPlayer) => otherPlayer.seat > playerSeat)
        // console.log("PLAYER SEAT IS: ", playerSeat)
        return (secondHalf.concat(firstHalf))
    }

    shuffleTableCards() {
        let rightCard = this.droppedCards.pop()
        //Deck is an object, therefore you have to access the inner array with this.deck.deck
        this.deck.deck = this.deck.deck.concat([...new Set([...this.droppedCards, ...this.history])])
        this.history = [rightCard]
        this.deck.shuffle()
        return this.deck
    }

    initiateTurn () {
        this.turn = this.players[0]
        return this.turn
    }

    validateGame(socketID) {
        //This is the logic for a 6 player game.
        let result = {socket: socketID, declare: true}
        let declaringPlayer = this.players.filter((player) => player.socket === socketID)[0]
        this.players.forEach((player) => {
            if ((player.score <= declaringPlayer.score) && (player !== declaringPlayer)) {
                result.declare = false
                this.players.filter((player) => player.socket === socketID)[0].tableStatus = false
            }
        })
        return result
    }
}

export { GameTable }