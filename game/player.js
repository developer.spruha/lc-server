class Player {
    constructor(username, socket, tableId = null, seat = null) {
        this.username = username
        this.socket = socket
        this.hand = []
        this.score = 0
        this.tableId = tableId
        this.seat = seat
        this.tableStatus = true
    }

    setSeat(seat) {
        this.seat = seat
    }

    dealCard (card) {
        this.hand.push(card)
    }

    getScore (card) {
        if (this.hand.length > 0) {
            let score = 0
            for (let i = 0; i < this.hand.length; i++){
                if (this.hand[i] === 'JB' || this.hand[i] === 'JR' || this.hand[i][1] === card[1]) {
                    continue
                } else if (this.hand[i][1] === 'J' || this.hand[i][1] === 'Q' || this.hand[i][1] === 'K' || this.hand[i][1] === '1') {
                    score += 10
                } else if (this.hand[i][1] === 'A') {
                    score += 1
                } else {
                    score += Number(this.hand[i][1])
                }
            }
            // console.log(`${this.username} has a score of: `, score)
            this.score = score
            return this.score
        } else {
            this.score = 0
            return this.score
        }
    }

    toggleTurn () {
        this.isTurn = !this.isTurn
    }
}

export { Player }