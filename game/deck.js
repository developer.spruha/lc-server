class Deck {
    constructor(limit = 2) {
        this.limit = limit
        this.deck = this.createDeck()
    }

    //Initializing the deck
    createDeck () {
        let newDeck = []
        let suites
        //Generating Two Decks for 6 players and One Deck for 2 players
        if (this.limit === 6) {
            suites = ['H', 'H', 'D', 'D', 'C', 'C', 'S', 'S']
        } else {
            suites = ['H', 'D', 'C', 'S']
        }
        let values = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K']
        for (let i = 0; i < suites.length; i++){
            for (let j = 0; j < values.length; j++){
                newDeck.push(suites[i] + values[j])
            }
        }
        // newDeck.push('JB', 'JR')
        // console.log(this.limit)
        // console.log(newDeck)
        return newDeck
    }

    //Shuffling the deck
    shuffle() {
        let currentIndex = this.deck.length, randomIndex;
    
        // While there remain elements to shuffle.
        while (currentIndex != 0) {
    
            // Pick a remaining element.
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex--;
    
            // And swap it with the current element.
            [this.deck[currentIndex], this.deck[randomIndex]] = [
                this.deck[randomIndex], this.deck[currentIndex]];
        }
        // console.log(this.deck)
        return this.deck;
    }

    //Picking a card out to deal or pickup
    drawCard () {
        return this.deck.shift()
    }

    deckLength () {
        return this.deck.length
    }
}

export { Deck }