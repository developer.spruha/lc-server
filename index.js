import { Deck } from './game/deck.js'
import { GameTable } from './game/gametable.js'
import { Player } from './game/player.js'
import { nanoid } from 'nanoid'
import express from 'express'
import http from 'http'
import cors from 'cors'
import { Server } from 'socket.io'

const app = express()

app.use(cors)

const server = http.createServer(app)

server.listen(3001, () => {
    console.log('Server is running.')
})

const io = new Server(server, {
    cors: {
        origin: "*",
        methods: ["GET", "POST"],
    }
})

let clients = []
let players = []
let games = []

io.on("connection", (socket) => {
    // if (clients.length === 2) {
    //     clients.shift()
    // }
    console.log('Client connected: ', socket.id)
    clients.push(socket.id)
    // console.log("Clients are: ", clients)

    socket.on('join_table', (username, tableId) => {
        socket.join(tableId)
        let newPlayer = new Player(username, socket.id, tableId)
        // if (players.length === 2) {
        //     players.shift()
        // }
        players.push(newPlayer)
        // console.log('Players are', players)
    })

    socket.on('room_joined', (tableId, limit) => {
        if (games.length === 0) {
            let gameTable = new GameTable(tableId, [], new Deck(limit), limit)
            games.push(gameTable)
        }

        games.forEach((game) => {
            if (game.tableId === tableId) {
                return
            } else {
                let newGameTable = new GameTable(tableId, [], new Deck(limit), limit)
                games.push(newGameTable)
            }
        })

        // console.log('Game tables before adding players: ', games)
        
        const gameTable = games.filter((game) => game.tableId === tableId)[0]

        // console.log('Game table joined', gameTable)
        //Setting seat numbers

        players.forEach((player, index) => {
            if ((player.tableId === gameTable.tableId) && (player.socket === socket.id)){
                player.setSeat(index + 1)
                gameTable.players.push(player)
            }
        })

        // console.log('Game table after pushing players: ', games)

        //We are starting game and initializing game table only after there are 2 players
        if (gameTable.players.length === gameTable.limit) {
            //Shuffling the deck.
            gameTable.deck.shuffle()
            //Selecting Joker and then dealing cards to players
            gameTable.selectJoker()
            //Initializing game table turn to player 1
            gameTable.initiateTurn()
            //Dealing cards to all players on gameTable
            gameTable.dealCards()
            //Adding one card to the right pile to get game started.
            gameTable.history.push(gameTable.deck.drawCard())

            io.to(tableId).emit('start_game')
            io.to(tableId).emit('send_joker', [gameTable.joker])
            io.to(tableId).emit('turn_is', gameTable.turn.socket)
            io.to(tableId).emit('send_history', gameTable.history)
        }

        socket.on('get_cards', () => {
            let player = gameTable.players.filter((player) => player.socket === socket.id)[0]
            socket.emit('send_cards', player)
        })
    
        socket.on('get_opponent', () => {
            let opponents = gameTable.sendTableOrder(socket.id)
            socket.emit('send_opponent', opponents)
        })

        socket.on('card_played', (card) => {
            //Have to update the hand of player who dropped a card.
            // gameTable.players.filter((player) => player.socket === socket.id)[0].hand.filter((item) => item != card)
            gameTable.players.filter((player) => player.socket === socket.id)[0].hand = gameTable.players.filter((player) => player.socket === socket.id)[0].hand.filter((item) => item != card)
            gameTable.droppedCards.push(card)
            // console.log('Dropped game table cards after playing: ', gameTable.droppedCards)
            io.to(tableId).emit('card_dropped', card)
            // io.to(tableId).emit('turn_is', newTurn.socket)
        })

        socket.on('add_to_history', () => {
            // console.log('Dropped cards before appending to history: ', gameTable.droppedCards)
            // console.log('History before adding any dropped cards: ', gameTable.history)
            if(gameTable.droppedCards !== []) {
                gameTable.droppedCards.forEach((card) => {
                    gameTable.history.push(card)
                })
            }
            // console.log('Added to history here: ', gameTable.history)
            io.to(tableId).emit('send_history', gameTable.history)
        })

        socket.on('get_score', () => {
            // console.log(globalJoker)
            // console.log(gameTable)
            let score = gameTable.players.filter((player) => player.socket === socket.id)[0].getScore(gameTable.joker)
            // console.log("Score is: ", score)
            socket.emit('send_score', score)
        })

        socket.on('draw_card', () => {
            // console.log('Game table deck on draw card: ', gameTable.deck)
            // let newCard = gameTable.deck.drawCard()
            //Checking if deck on table is completed.
            if (gameTable.deck.deckLength() !== 1){
                gameTable.players.filter((player) => player.socket === socket.id)[0].hand.push(gameTable.deck.drawCard())
                // console.log('draw card successful!')
            } else {
                //Reshuffle table cards here.
                gameTable.shuffleTableCards()
                // console.log(gameTable.deck)
                gameTable.players.filter((player) => player.socket === socket.id)[0].hand.push(gameTable.deck.drawCard())
                // console.log('draw card successful!')
            }
            let newHand = gameTable.players.filter((player) => player.socket === socket.id)[0].hand
            socket.emit('take_card', newHand)

            //Ending the turn and changing after drawing a card.
            gameTable.toggleTurn()
            // let newTurn = gameTable.toggleTurn()
            io.to(tableId).emit('turn_is', gameTable.turn.socket)
        })

        socket.on('draw_card_from_second', () => {
            // console.log('Game dropped cards before drawing a card from second pile: ', gameTable.droppedCards)
            // console.log('Game history cards before drawing a card from second pile: ', gameTable.history)
            let newCard = gameTable.history.slice(-1)[0]
            // gameTable.history = gameTable.history.filter((card) => card != newCard)
            // console.log('Game history cards after drawing a card from second pile: ', gameTable.history)
            // console.log('Card drawn from second pile is: ', newCard)
            gameTable.players.filter((player) => player.socket === socket.id)[0].hand.push(newCard)
            let newHand = gameTable.players.filter((player) => player.socket === socket.id)[0].hand
            socket.emit('take_card', newHand)

            //Ending the turn and changing after drawing a card.
            gameTable.toggleTurn()
            // let newTurn = gameTable.toggleTurn()
            io.to(tableId).emit('turn_is', gameTable.turn.socket)
        })

        socket.on('shift_dropped_pile', () => {
            // console.log("Data after shifting dropped pile: ", gameTable.droppedCards)
            let freshPile = gameTable.droppedCards.slice(-1)
            // console.log('Shifting the new pile here: ', freshPile)
            // gameTable.history = gameTable.history.push(freshPile[0])
            // console.log('After shifting pile, game history: ', freshPile)
            io.to(tableId).emit('clear_table', freshPile)
        })

        socket.on('end_turn', () => {
            gameTable.toggleTurn()
            // let newTurn = gameTable.toggleTurn()
            io.to(tableId).emit('turn_is', gameTable.turn.socket)
        })

        socket.on('show_cards', () => {
            let player = gameTable.players.filter((player) => player.socket === socket.id)[0]
            let opponent = gameTable.players.filter((player) => player.socket != socket.id)[0]
            let playerScore = player.getScore(gameTable.joker)
            let opponentScore = opponent.getScore(gameTable.joker)
            // console.log('Player score is: ', playerScore)
            // console.log('Opponent score is: ', opponentScore)
            if (gameTable.limit === 2){
                if (playerScore < opponentScore) {
                    io.to(tableId).emit('winner_is', player.socket)
                } else {
                    io.to(tableId).emit('winner_is', opponent.socket)
                }
            } else {
                let result = gameTable.validateGame(socket.id)
                io.to(tableId).emit('send_declaration', result)
                //Toggle Turn After Declaration to keep game going if wrong show
                gameTable.toggleTurn()
                io.to(tableId).emit('turn_is', gameTable.turn.socket)
            }
        })

        //If a user declares but is incorrect on the table.
        socket.on('exit_game_table', (tableID) => {
            gameTable.players.filter((player) => player.socket === socket.id)[0].tableStatus = false
            let activePlayers = gameTable.players.filter((player) => player.tableStatus === true)
            if (activePlayers.length === 1) {
                socket.to(activePlayers[0].socket).emit('direct_victory')
            }
            io.to(tableId).emit('ask_for_opponents_again')
        })

        socket.on('disconnect', () => {
            gameTable.players.filter((player) => player.socket === socket.id)[0].tableStatus = false
            let activePlayers = gameTable.players.filter((player) => player.tableStatus === true)
            if (activePlayers.length === 1) {
                socket.to(activePlayers[0].socket).emit('direct_victory')
            }
            if (gameTable.turn === gameTable.players.filter((player) => player.socket === socket.id)[0]){
                gameTable.toggleTurn()
                io.to(tableId).emit('turn_is', gameTable.turn.socket)
            }
            io.to(tableId).emit('ask_for_opponents_again')
        })
    })

    socket.on('disconnect', () => {
        console.log('Client disconnected: ', socket.id)
        clients = clients.filter((socketID) => socketID != socket.id)
        players = players.filter((player) => player.socket != socket.id)
    })
})
