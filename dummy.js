//4 different types of scopes. Global Scope, Module Scope, Function Scope, Block Scope
//You have to worry about Module Scope and Block Scope most of the time.
//

const a = 1

function test () {
    const b = 2
    console.log("Function: ", a, b)
}

test()
// console.log(a, b) 
//ERROR b is not defined. It is defined only in function. It is only accessible within that function.

const globalVar = "Global"

//Global variables should be minimal. It becomes hard to track changes and it pollutes code. Use modular scope more often.

const moduleVar = "Module"

export const exportedVar = moduleVar

//import { exportedVar } from 'dummy.js' can be used in other files and it works. 

//Block Scoping

if (true) {
    const c = 3
    //Can access C here only.
}

//Cannot access C here. 

//ANY TIME YOU SEE CURLY BRACES, IT IS BLOCK SCOPE. YOU MUST MAKE IT GLOBAL OR MODULAR.
//Values instantiated within blocked scopes can be accessed within child nested blocks, but not parent.

// let and const are block level scopes. 
// a variable instantiated with 'var' and within a function ignored {block scope} and can be called anywhere within the function.
//99% of the time, do not use 'var'. You should use const and let.